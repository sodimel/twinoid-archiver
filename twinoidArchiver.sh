#!/bin/bash

clear
printf "\n"
printf "     --.--       o             o     |    ,---.             |     o\n"
printf "       |   . . . . ,---. ,---. . ,---|    |---| ,---. ,---. |---. . .    , ,---. ,---.\n"
printf "       |   | | | | |   | |   | | |   |    |   | |     |     |   | |  \  /  |---' |\n"
printf "       '   '-'-' ' '   ' '---' ' '---'    '   ' '     '---' '   ' '   ''   '---' '\n"
printf "\n\n"

if [ $# -eq 0 ]
	then
		printf "This script will download all your public posts & likes in a big html file in \e[92mfolder_name\e[39m,\n"
		printf "then will split this big file in multiple files (xxx-\e[92mfolder_name\e[39m.html), so you can see the\n"
		printf "content without needing to allow 8Gb ram to your browser."
		printf "\n"
		printf "Usage:\n"
		printf "./twinoidArchiver.sh \e[93mid \e[92mfolder_name \e[93msid \e[92mchk\e[39m\n"
		printf "\n"
		printf "    \e[93mid\e[39m             profile id in url (\e[2mhttps://twinoid.com/user/\e[0mid <- here)\n"
		printf "    \e[92mfolder_name\e[39m    name of the folder (user name ?)\n"
		printf "    \e[93msid\e[39m            connection cookie value\n"
		printf "    \e[92mchk\e[39m            check value (found in requests)\n"
		printf "\n"
		printf "\e[2mFor detailed instructions, see \e[0mhttps://git.bitmycode.com/sodimel/twinoid-archiver\e[2m.\e[0m\n"
		exit 1
fi

if [ $# -ne 4 ]
	then
		printf "Invalid number of args !\n"
		printf "\e[2mSee the help with launching the script without args (\e[0m./twinoidArchiver.sh\e[2m).\e[0m\n"
		exit 1
fi

printf "Downloading...\n"

mkdir $2
cp style.css $2/style.css
cd $2
curl "https://twinoid.com/mod/wall/user/$1?chk=$4;infos=og;page=15000000000;before=1000000000;_id=tid_2;jsm=1;lang=fr;host=twinoid.com;proto=https%3A;sid=$3" -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0' -H 'Accept: */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'DNT: 1' -H 'Connection: keep-alive' -H "Referer: https://twinoid.com/user/$1" -H "Cookie: tw_sid=$3; tp_login=1" > $2.html

printf "Download complete!\n"
printf "Minimizing file...\n"

sed -i 1d $2.html # remove first line

sed -i 's/\\n//g' $2.html
sed -i 's/\\r//g' $2.html
sed -i 's/\t//g' $2.html
sed -i 's/ likeOnly" id="wallEvent_[a-zA-Z0-9]*">//g' $2.html
sed -i 's/" id="wallEvent_[a-zA-Z0-9]*">//g' $2.html

# saut de ligne pour chaque "post"
sed -i 's/<div class="wallEvent/\n/g' $2.html

printf "Done!\n"
printf "Splitting file...\n"

split $2.html -l 300 --additional-suffix=-$2.html

printf "Done!\n"
printf "Adding header...\n"

for file in ./x*.html; do
	mv $file $file-temp.txt
	cat ../header.html $file-temp.txt > $file
	rm $file-temp.txt
done

printf "Done!\n"
printf "Thanks for using Twinoid Archiver :)\n"
