
![icon](icon.png)


Twinoid archiver is a small bash script that will download all of the posts and likes of your public profile.

#### Prerequisites

You will need `bash` & `curl` to execute this script.

## Usage


 1) Download the archive:
    ```
    git clone https://git.bitmycode.com/sodimel/twinoid-archiver.git
    ```
 2) Get your `sid` & `chk` cookie values from `right click`>`inspect`>`network` tab (scroll down, then search for `chk`): [video](https://up.l3m.in/file/1584879222-twinoid-archiver.webm).

3) Launch the script:
    ```
    ./twinoidArchiver.sh id folder_name sid chk
    ```

4) Go to `folder_name`, open `folder_name.html` (if you dare! It can freeze your computer), or open any of the `xxx-folder_name.html` file.

#### License

WTFPLv2, feel free to use/modify this program as you want.

#### Credits

"Twinoid" logo is property of Motion Twin.

#### Disclaimer

*Twinoid's terms and conditions prohibit the use of this kind of script. Therefore, it is presented here for information purposes only, to help you better understand how twinoid & bash works.*
